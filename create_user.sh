#! /bin/bash

# Usage: 
# ./create_user.sh verentsov ssh-rsa AAAA...adsASHJ== msa@asjdk.ask
# or 
# ./create_user.sh verentsov https://gist.githubusercontent.com/sVerentsov/c460c62295d1a33108d74420921e56d1/raw/e490ed6943eb69b71a3fd9088969d9b643d89585/authorized_keys"

USER="$1"
shift 
SSH_PUBLIC_KEY="$*"
useradd -m ${USER}
usermod -aG sudo ${USER}

if [[ $SSH_PUBLIC_KEY == ssh-rsa* ]];
then 	
	su - ${USER} -c "umask 022 ; mkdir .ssh ; echo $SSH_PUBLIC_KEY >> .ssh/authorized_keys"
else 
	su - ${USER} -c "umask 022 ; mkdir .ssh ; wget -O  .ssh/authorized_keys $SSH_PUBLIC_KEY"
fi

passwd -de ${USER}
