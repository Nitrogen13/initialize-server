apt-get update 
apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs)  stable"
apt-get update
apt-get install -y docker-ce docker-ce-cli containerd.io

curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
apt-get install -y gitlab-runner
gitlab-runner register -n --url https://gitlab.com/ --registration-token $GITLAB_TOKEN --executor shell --description "My Runner"
usermod -a -G docker gitlab-runner
sh -c "echo 'gitlab-runner ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers"