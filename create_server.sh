# install packages 
apt update
apt install -y git vim tmux software-properties-common ufw net-tools htop
add-apt-repository -y ppa:deadsnakes/ppa
apt install -y python3.7-dev
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3.7 get-pip.py
python3.7 -m pip install rmate

# install oh-my-zsh

apt install -y zsh
chsh -s /usr/bin/zsh root
wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh
sed -i 's/DSHELL=\/bin\/bash/DSHELL=\/bin\/zsh/' /etc/adduser.conf
sed -i 's/SHELL=\/bin\/sh/SHELL=\/bin\/zsh/' /etc/default/useradd
sed -i 's/\/root\/.oh-my-zsh/$HOME\/.oh-my-zsh/' /root/.zshrc
cp -r /root/.oh-my-zsh /etc/skel/
cp /root/.zshrc /etc/skel

#Change ssh port
sed -i 's/#Port 22/Port 2763/g' /etc/ssh/sshd_config

# Disable SSH root login
sed -i "s/#PermitRootLogin prohibit-password/PermitRootLogin no/g" /etc/ssh/sshd_config
sed -i "s/PermitRootLogin prohibit-password/PermitRootLogin no/g" /etc/ssh/sshd_config
sed -i "s/#PermitRootLogin yes/PermitRootLogin no/g" /etc/ssh/sshd_config
sed -i "s/#PermitRootLogin Yes/PermitRootLogin no/g" /etc/ssh/sshd_config
sed -i "s/#PermitRootLogin YES/PermitRootLogin no/g" /etc/ssh/sshd_config
sed -i 's/LoginGraceTime .*/LoginGraceTime 60/g' /etc/ssh/sshd_config
sed -i 's/PermitRootLogin .*/PermitRootLogin no/g' /etc/ssh/sshd_config
sed -i 's/Protocol .*/Protocol 2/g' /etc/ssh/sshd_config
sed -i 's/#PermitEmptyPasswords .*/PermitEmptyPasswords no/g' /etc/ssh/sshd_config
sed -i 's/PasswordAuthentication .*/PasswordAuthentication no/g' /etc/ssh/sshd_config
sed -i 's/X11Forwarding .*/X11Forwarding no/g' /etc/ssh/sshd_config
service sshd restart

# setup ufw 
ufw allow 2763
ufw --force enable

# add user
useradd -m ilgizar
usermod -aG sudo ilgizar
su - ilgizar -c "umask 022 ; mkdir .ssh ; wget  -O .ssh/authorized_keys https://gist.githubusercontent.com/Nitrogen13/e86e4d71284352ccea85faec95ba7227/raw/8bfe48c9c0cf9cec645ba15ae2167daa00a6fcd9/key.pub"
#prompt user to create password on first login
passwd -de ilgizar

wget https://gitlab.com/Nitrogen13/initialize-server/raw/master/create_user.sh
sh ./create_user.sh alexey https://gist.githubusercontent.com/sVerentsov/95a1243d3c3bc65203f96d344299dc39/raw/6ed1c31e9fd6200761ab369bd9f03b3575ab87e7/a.kazantsev
sh ./create_user.sh david https://gist.githubusercontent.com/sVerentsov/95a1243d3c3bc65203f96d344299dc39/raw/6ed1c31e9fd6200761ab369bd9f03b3575ab87e7/d.moiseev
sh ./create_user.sh aidar https://gist.githubusercontent.com/sVerentsov/95a1243d3c3bc65203f96d344299dc39/raw/6ed1c31e9fd6200761ab369bd9f03b3575ab87e7/a.valeev
sh ./create_user.sh herman https://gist.githubusercontent.com/sVerentsov/95a1243d3c3bc65203f96d344299dc39/raw/6ed1c31e9fd6200761ab369bd9f03b3575ab87e7/h.tarasau
